#### Probability

The probability of streaks is outlined in [this math overlflow question and answer](https://math.stackexchange.com/questions/383704/probability-of-streaks).
This was a problem when calculating the amount of spins on a roulette wheel before a martingale betting strategy fails.
