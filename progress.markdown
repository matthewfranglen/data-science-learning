Spend one hour a day on this.

Split time between:

 * Fast AI course
 * CMU NLP course
 * NLP Book
 * Deep Learning website
 * Probability
 * Papers
 * Implementation

The aim will be to spend half of the time on the courses and books and half the time on actual implementation.

## Fast AI

[x] lesson 1-7
[x] lesson 8
[ ] lesson 9

Initialize a network manually using the techniques outlined in [All you need is a good init](https://arxiv.org/abs/1511.06422)

## CMU NLP course

https://github.com/neubig/nn4nlp-code

[x] lesson 1
[x] reading for lesson 1 - chapters 1-5 of the Neural Network book (Yoav Goldberg).
    http://phontron.com/class/nn4nlp2019/schedule/class-introduction.html

[x] reading for lesson 2 - chapters 8-9 of the Neural Network book (Yoav Goldberg).
    http://phontron.com/class/nn4nlp2019/schedule/predicting-the-next-word.html
[x] chapters 6-7 of the Neural Network book are a reference for the second lesson
[x] lesson 2
In this he describes perplexity w.r.t. language models as the number of times you would have to guess before you chose the correct following word.
So if you had a model with a perplexity of 100, then on average you would have to guess 100 times before choosing the correct next word.
Towards the end he answers a question about picking the batch size.
He points out that the batch size is a hyperparameter which affects the stability of the model.
A bigger batch size will be more stable (won't bounce around so much).
It can also be slower to converge at the beginning, this really comes down to how fast you can compute the batch.
At the beginning the loss of stability of having small batches is made up for by having faster convergence (when considering wall clock time).
The stability of large batches can also cause you to get stuck in local optima more easily.
The batch size can be changed during training!
Can start small and get bigger.

I've completed this class now.
Reading the papers in advance was good however it then made much of the content of the class less impactful.
It may be better to watch the video and then read the papers.



There are also 10 papers to read.
I should try to read two per day and that will give the book time to arrive.

[x] I've read: Reference: A Neural Probabilistic Language Model. (Bengio et al. 2003, JMLR) http://www.jmlr.org/papers/volume3/bengio03a/bengio03a.pdf
    It describes using a neural network to create a language model with the aim of predicting the next word.
    The neural network is compared to n-gram based approaches.
    The description of the work states that the neural network is able to generalize to similar words (e.g. dog / cat) while the n-gram approach is not.

I should get better at reading the papers.
Perhaps just the abstract / introduction / results / conclusion?

[x] Reference: An Overview of Gradient Descent Algorithms. (Ruder 2016) https://ruder.io/optimizing-gradient-descent/index.html#whichoptimizertochoose
    This is a discussion of gradient descent algorithms.

[x] Reference: The Marginal Value of Adaptive Gradient Methods. (Wilson et al. 2017) https://arxiv.org/pdf/1705.08292.pdf
    This is a review of the gradient descent algorithms.
    This states that SGD like approaches can perform better than the optimizers that adapt parameters individually like adam.
    Best SGD alike would be nesterov.

[thoughts on reading style]
    This did seem to cover most of the important stuff.
    (I am going to go back and read the background and body of the paper now.)
    It's good to review what I missed to see if this approach has merits.
    This approach did cut the paper size approximately in half.
    I think the general approach is sound.

[x] Reference: Stronger Baselines for Neural MT. (Denkowski   Neubig 2017) https://www.aclweb.org/anthology/W17-3203.pdf
    This covers advances in NLP and how the experimental systems designed by researchers may be difficult to compare to production systems.
    The abstract talks about three methods which produce "much stronger experimental systems" - presumably this means closer to production or more comparable.
    The core problem is evaluating techniques on very simple toy models may not accurately reflect the same changes in more complex production systems.
    After all the problems identified in simple systems may've been solved in a different way to make the production system.

[x] Reference: Dropout. (Srivastava et al. 2014) http://jmlr.org/papers/volume15/srivastava14a.old/srivastava14a.pdf (30 pages!)
    This is the paper that introduced dropout.
    Dropout is the process of randomly excluding parameters in a network during training to prevent over specialization.
    This results in a network that generalizes better.

[x] Reference: Dropconnect. (Wan et al. 2013) (link is access forbidden, found http://yann.lecun.com/exdb/publis/pdf/wan-icml-13.pdf instead)
    This paper introduces dropconnect which is a generalization of dropout.
    This is intended for regularization of large fully connected layers.
    The difference is that instead of zeroing the activation, dropconnect zeros the _weights_.

[x] Reference: Marginal Value of Adaptive Gradient Methods (Wilson et al. 2017) https://papers.nips.cc/paper/7003-the-marginal-value-of-adaptive-gradient-methods-in-machine-learning.pdf
    Wait a second! I've read this already!
    Why does it appear twice in the reference list (and with different links)?

[x] Reference: Using the Output Embedding. (Press and Wolf 2016) https://arxiv.org/pdf/1608.05859.pdf
    This is a paper that discusses how the topmost matrix of a neural network language model constitutes a valid embedding.
    It starts by describing how the input to a network is converted into an embedding by the first layer of the network.
    It is then this embedding which the network operates over (by iteratively computing new matrices), producing probabilities in the final layer.
    The paper points out how similarities in the behaviour of these embeddings is desirable - e.g. synonyms in the input should be treated similarly, and synonyms in the output should have similar scores.
    So the input embedding and the output embedding can be the same set of parameters (weight tying).

[ ] Reference: Regularizing and Optimizing LSTM Language Models. (Merity et al. 2019) https://openreview.net/pdf?id=SyyGPP0TZ


[ ] Assignment 1 - Text Classifier / Questionnaire
implement a text classifier and fill in questionnaire project topics

[ ] Assignment 2 - Survey of the State of the Art
Survey your project topic and describe the state of the art
* existing methods
* advantages
* disadvantages

[ ] Assignment 3 - Reimplement SOTA
Reimplement and reproduce results from a state of the art model

[ ] Assignment 4 - Final Project
Implement unique project that either improves on SOTA or applies NN to unique task

## Deep Learning Website

[ ] Introduction
[ ] Linear Algebra

## Probability

[x] Discrete Probability
[ ] Complete calculation of martingale
[ ] Continuous Probability
