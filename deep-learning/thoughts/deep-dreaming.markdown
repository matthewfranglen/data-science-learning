I can make deep dreaming more useful by doing the following:

* Construct a network C that classifies an image into multiple classes
* Construct a "network" I that has parameters that are the image and an identity input
* Train network I using C as a loss function

This would allow an image to be permuted to make it more "gorilla like" (or whatever).
This may well permute the image outside the desired bounds - if I want a racist classification then I need to bound the change to an existing face.

So why don't I permute network I so that only _part_ of the image comes from the parameters.
That way I can restrict what is altered.
The parameters can be identity values except for the area of the image that we wish to permute.
