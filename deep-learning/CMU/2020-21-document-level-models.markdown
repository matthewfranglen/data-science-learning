https://www.youtube.com/watch?v=K72U5dlPwkY&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=20

CMU Neural Nets for NLP 2020 (21): Document Level Models

How can you extrapolate sentence level processing to an entire document?

## Predicting over Documents

Document level language modelling
  - predicting the next word based on the preceding document

Document classification
  - predicting traits of entire documents

## Document Structure

Entity coreference
  - tracking entities across the entire document

Discourse parsing
  - finding correspondences between different sections

## Document Level Language Modelling

### RNN

Things like RNNs used recurrent models.
Could just extend this to the entire document.

The problem is that it is difficult to pass data from sentence to sentence.
The number of steps between them is great so information is easy to lose.
Can have a separate encoding for coarse grained document context.

How do you choose what to include in the document level context?
 - Topic modelling of the previous sentence
 - Bag of words of the previous sentence
 - Can use last state of the previous sentence (keep on passing the same state, separate to the sentence state, through the entire sentence)

### Self Attention / Transformers

Self attention and transformers are harder as they are fixed size.
Can just include the previous sentence(s) and make the attention / transformer layer big enough.
Could include the previous sentence as a second dimension and operate over a matrix.
This has big implications for the computational complexity though.

### Transformer-XL

This is a combination of recurrent and transformer models.
Can attend to fixed vectors from the previous sentence (like the previous sentence state?).

This works well computationally because it limits the amount of additional work that has to be done.

### Sparse Transformers

Adding a "stride" to the attention, so you attend to only the previous N states.
The stride changes per layer, which then allow you to cover all the inputs.

## Document Level Evaluation

Simple - perplexity
More focused - scrambling sentences and attempting to recover the original order.
Final sentence prediction - between two possible options. Could be hard to create valid endings.
Final word prediction - should require long distance context.

## Entity Coreference

Identify noun phrases mentioning an entity.
They don't necessarily need to identify the entity (e.g. him, her).

Then need to tie together the references to the same entity.

The approach to tying together the entities is hard.
The search space is N**2.

Mention detection is hard, and you then need to pair them correctly.
Need to have something that can train the weights all the way back to mention detection.
Still want to be able to limit the number of comparisons that are performed.

If you remove candidates when limiting, then you have a problem.
How do you backpropagate through the pruning operations?
The gradients can flow through the included entities which may be enough.

## Discourse Parsing

Discourse unit - small pieces of text (there is a proper definition).
Determine the relationships between the discourse units.

To create a hierarchy of discourse units a hierarchical deep model could be used, like the pos tagger.
