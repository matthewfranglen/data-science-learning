https://www.youtube.com/watch?v=4SjdBB64mjo&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=16

CMU Neural Nets for NLP 2020 (17): Adversarial Methods

## Generative Models

Model a data distribution, or a conditional one.
A perfect generative model:

 * Should be able to determine likelihood - know the probability distribution
 * It should be able to generate samples
 * And infer latent attributes - like the topic of a sentence

### Maximum Liklihood Estimation vs Generative Adversarial Method

MLE is good at likelihood but weaker at generation and cannot infer latent variables at all.

GAN is very poor at likelihood but very good at generation and can infer latent variables a bit.

## Adversarial Training

Create a discriminator that criticizes the output in some way.
Either is the output accurate or not, or is there some feature that is distinctive about it?

### Generator and Discriminator

Discriminator: is the output real?
Generator: try to fool the discriminator

Discriminator is trained on the generator output and on the real output.
Then generator is trained to fool the discriminator - the degree to which the discriminator believes the generator output is the loss of the generator.

The discriminator can end up identifying a trait of the model output - like blurriness - that a regular loss would not detect.
The discriminator as a loss function also provides a lot of feedback to the GAN on how to improve.

### Problems

It's very difficult to train a GAN effectively.

Known problems:

 * Convergence and Stability - training is hard and does not converge
   WGAN / Gradient Based Regularization

 * Model collapse/dropping - could just generate an image from the training data, rather than generating a new image
   Can detect if the output of the model is very similar across all outputs, and can lead to a penalty

 * Overconfident discriminator - at beginning of training discriminator will be very confident, leading to gradient collapse
   Can smooth the labels to remove the confidence

### Applying to Text

GANs can be used for text.
The problem is that the output is discrete the back propagation does not work.
 - can work with the softmax output which is not discrete (but is almost)

Also need a way to create a discriminator for sequences.

### Discriminator against hidden features

Can train on texts from different domains (reviews of books vs reviews of films) and then use a discriminator to try to determine the domain from the hidden feature.
The aim here is to strip the knowledge of the domain from the model that is being produced.
This should then allow the model to generalize across the domains better.

The problem here is that the different domains may have legitimate differences in distribution in the aspect you care about (e.g. sentiment).
This would then mean that a given value from that distribution could be used to infer the domain.
You don't want to strip this information because it is your primary task.

### Break Classifier

Work out what change to the image would maximize the loss change.
This can be done to text too, which can then lead to things like machine translation breaking drastically.

Need to find sentences that are close together semantically but will produce dramatically different outputs.
The problem with permuting text is that very small changes can lead to legitimately different texts (psychologist psycopath are close in some languages).

The aim is to preserve the meaning on the input, but break the meaning of the output.
This can be achieved by measuring similarity of original input to permuted input, and same on the output side.
Minimize the difference on input and maximize it for the output.

Semantic similarity for text is hard to define.

Would like to be able to train to handle adversarial input.
Can generate adverserial data at training time, as it is based on maximizing the difference in the output for a small change in input.

## How can you determine likelihood

If you could calculate the input that generates a given output then you could determine the probability of the given input.
This is very hard, so GANs are very poor at estimating likelihood.likelihood
