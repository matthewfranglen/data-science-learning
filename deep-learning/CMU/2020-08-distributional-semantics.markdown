CMU Neural Nets for NLP 2020 (8): Distributional Semantics and Word Vectors
https://www.youtube.com/watch?v=RRaU7pz2eT4&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=7

Pre Trained Word Representations
--------------------------------

Can create embeddings for words and then for sentences.
How do you train them?

You can use existing embeddings that have already been trained.
Can come from a language model, or from a task specific network (e.g. POS tagging - a supervised task).
Even if the task is supervised, the embeddings are not (there is no guidance on the correct embedding - just on the overall task).

### Non Contextualized Word Representations

What do we want to know about words that isn't directly contained in the word

 * Do they mean the same thing
 * How do they relate
 * What part of speech
 * What conjugation
 * ...

embeddings can address some of this.
The non contextualized representation creates an embedding for a word independent of the other words in the sentence.
This means that the embeddings for individual words do NOT have context.

#### Word Net

Word Net is a large database of words with parts of speech and semantic relations etc.
Big network of is-a relationships.

It took a lot of effort to create, is in multiple languages.
It's currently not being maintained.

How could we recreate it?
With embeddings?

The parts of the embedding can encode different aspects of a word.
The embedding space can even be (partially) euclidean (king - man + woman = queen).

#### Distributional vs Distributed Representations

Distributional Representations - words are similar if they appear in similar contexts
  Non distributional representation might be a dictionary or word net (no context info)
  linguists do this to understand the meaning of words

Distributed Representations - embeddings
  local representations (one hot vector) to actual embedding (continuous vector)

embedding is a _distributed_ representation (I sorta expected distributional as embeddings for similar words should be similar).

##### Distributional

Can create a word-context count matrix
  Count number of co occurrences of word and context
  Lots of problems with naive approach - need to handle missing entries

#### Embeddings from Language Models

Can take it from the first layer of the network, where token is translated into matrix input
However restricting yourself to a language model restricts the possible embeddings
If you don't have to calculate probability of next word then other options are available

Can use the sum of the context words with a weight (cbow) - allows you to predict masked words in the middle, as context goes both ways.
It's a continuous bag of words so it has all the downsides of that.
Cannot share the context embeddings with the word embeddings for a simple model like cbow as they are calculating different things.

Can try to calculate the context from the word - a harder problem that is like a mass language model.

Word 2 Vec was the same technique that had been used before however it had optimizations that let it be used on much bigger datasets.
Word 2 Vec uses SGD to train which wasn't used before.

GloVe based on ratios of P(word | context) probabilities (e.g. P(solid | ice) / P(solid | steam)) which shows the strongly related words.
Also uses SGD.

Train embeddings from scratch when the data for the task is different to the data that was used to train the pretrained embeddings.

#### What context to use?

Context has a large effect
small context window -> more syntax based (regular syntactic structure breaks down after a small number of words - locality in syntax)
large context window -> more semantic based

#### How to evaluate embeddings?

intrinsic evaluation - how good are the features it identifies
  can reduce the high dimensional embeddings into 2 or 3 dimensions so you can visualize them
  e.g. country to capital city, are the vectors similar?

  Can use non linear projections - e.g. SNE/t-SNE which can better cluster the data
  hyperparameters can make a big difference, it can make things worse
  t-SNE and other non linear projections BREAK VECTORS - it is not euclidean anymore

  relatedness - can evaluate the similarity of word embeddings to human evaluation of word similarity
  analogies
  categorization

extrinsic evaluation - how well does the overall model perform with the embeddings
  can just use the embedding and see
  can concatenate pretrained embeddings with the learned ones

There is no one best embedding, may have to evaluate several different embedding training approaches

#### When are they useful

When the training data is insufficient to train the whole model from scratch
very useful - tagging, parsing, text classification
less useful - machine translation
not useful - language modelling (you have enough data or you can't create the model anyway)

#### Limitations

Embeddings can be sensitive to very small differences (e.g. dog -> dogs)
May not be coordinated across languages
Difficult to interpret
Can encode bias that is present in the source text

#### Sub word embeddings

Can capture sub word regularities
Is this just working out how words are formed

This can help with the dog -> dogs thing as it could be {dog, s}

Can split the word into n-grams and then use those as the sub-words
Then sum the embeddings to get the embedding for the overall word
(can include start-of-word / end-of-word grams too)

#### Fitting embeddings to lexicons

Can use something like word net as an evaluation to ensure that the embeddings encode the information in the lexicon
e.g. ensure that two synonyms have similar embeddings

#### Bias

Can find bias by using the euclidean behaviour of the space to find those professions that are closest to he or she
Can also go from one profession to the "opposite gender" profession
