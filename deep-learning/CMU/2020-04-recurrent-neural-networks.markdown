[CMU Neural Nets for NLP 2020 (4): Recurrent Neural Networks](https://www.youtube.com/watch?v=wD-mB2clN_0&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=4)

CMU Recurrent Neural Networks
-----------------------------

Watching this with Joe.

RNNs are good because NLP is full of sequential data.
There are also relationships between distant words.

When doing speech recognition have to deal with homonyms (words that sound the same).
Apparently this is done in a two phase process - collect the sounds as words, then fix the words.

"it" is a reference which can vary based on knowledge of the world.
 - The trophy would not fit in the suitcase because it was too big - it refers to trophy
 - The trophy would not fit in the suitcase because it was too small - it refers to suitcase

RNN is recursive however it is still a DAG
    (this is a bit like self referential generic definitions - it is finite at runtime)

Could you back propagate from each step?
    The first label loss gets feedback from the whole list while the last one only gets feedback from itself.
    This would make training uneven.

BiRNN run it in both directions and then concat the two outputs.
    Cannot use this for language modelling as you are trying to predict the next word, which is required as the input for the backwards pass.

Vanishing Gradients
    The stddev / mean of each round can cause the gradients to go to infinity or disappear.
    It would be super bad to explode, so it likely trains against that?
        Gradients exploding can be handled easily by fixing their absolute magnitude to a fixed amount (gradient clipping).
    The input from early words can disappear though.
    The activation function can force the _gradient_ to be less than one!

LSTM helps fix this
    The state changes according to additive connections between timesteps
    Addition always has a gradient of 1

LSTM works by having a heavily modified mask which is then combined with the current input to update the state.
The state is always carried forward without any other modification, so it changes more slowly.
The mask changes considerably, more than in a RNN (it changes based on the current input and the current state).

Some parts of the model (of any model) can be removed without affecting performance.
(The lottery ticket paper)

RNNs can calculate arbitrary functions (Siegelmann and Sontag 1992).

Handling batching
    Need to have input / output of the same size
    Can add padding
    Can use a mask to indicate the values that matter
    Many frameworks can handle this automatically
    Doing the back propagation is tricky - don't want to calculate it on the padding or masked elements
    Can sort the sentences to have batches of approximately the same size, stops having to process too many masked or padded elements
    Sorting like this _does_ have an effect on the training results

RNNs are slow on GPUs as they are inherently sequential
    There are optimal versions built into CUDA that will be extremely fast, however you cannot then modify the core RNN/LSTM as it loses efficiency
    Could learn to program CUDA

Gated Recurrent Unit (GRU)
    Has hidden state, but is updated in a much more simple way
    Cannot count number of words

Can have RNNs of different scales (having broader connections, pyramidal)
    This might be a bit like the feature pyramid / inverse pyramid for CNNs
    The underlying nodes are concatenated together to form the input for the next layer of RNN
    It's biRNN in the example given

How to handle long inputs that have references
    You can batch and then preserve the state and pass that as the initial state to the next batch
