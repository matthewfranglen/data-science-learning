https://www.youtube.com/watch?v=-I-3qRg3ExI&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=9
CMU Neural Nets for NLP 2020 (10): Debugging Neural Nets (for NLP)

## Debugging is very important!

Models are complicated and opaque

Everything is a hyperparameter, and can directly affect the results

Non convex landscapes with gradient descent does not guarantee decreasing loss or convergence

### Understand the problem

Normally:

 * Implement model
 * Code looks ok
 * Accuracy on test set is bad though :(

Then what?

Randomly change lines of code?
BAD

### Possible causes

Training time problems
  - does your training loss go down?
  - does your model have capacity?
  - is there a bug in the model?

Decoding time problems
  - disconnect between training and test
  - failure of search algorithm

Overfitting

Mismatch between optimised function and evaluation

Don't debug everything at once!

### Debugging at Training Time

Look at the loss function on the training set!
  - Is it going down
  - Is it going down to zero (after a long enough time)
  - If it doesn't go to zero on big data, what about very small data?

#### Weak Model

The model might be too weak.
The size of the model dictates how much it can do.

For language modelling you need at least 512 nodes (in a layer) before you get resonable results.
For language analysis you need at least 128
  These are minimums!

Multiple layers are better.
For longer sequences (e.g. characters instead of words) you need larger layers - as it has to remember for longer

You need enough capacity to match the label space for the output.

Should you add more units to a layer or add more layers?
  More than 1k nodes in a layer rarely leads to better results
  More layers does lead to better results
  Theoretically a one layer network (perceptron) can calculate anything

More layers can be problematic - vanishing gradients.
Residual connections (skip connections) can help.
GRU version -> highway networks, the masked version of either current output or provided state
  You must use something like this!

#### Bad Optimizer Strategy

If bigger models don't help then optimisation strategy may be off.

 * Bad optimizer choice
  - adam with default lr is good start
  - otherwise look for prior art in this area
  - maybe fiddle with lr or batch size
 * Bad learning rate
 * Bad initialization
 * Bad minibatching strategy

Learning Rate
 * Too low - learn very slowly
 * Too high - learn for a bit then fluctuate and diverge

Can use annealing to allow the high rate to work at the start.
SGD is not guaranteed to converge unless you decrease the learning rate.
Adam has this built in a little.

Optimisers have different learning rates!

(transformer without tears <- google this paper)
https://arxiv.org/abs/1910.05895
How to implement transformers nicely.

LSTM: A Search Space Odyssey <- exhaustively tried different LSTM variants, original was good
https://arxiv.org/abs/1503.04069

#### Initialization

Neural nets are sensitive to initialization - results in different sized gradients

Gaussian initialization - zero mean gaussian distribution used
Uniform range initialization - uniform distribution within a range
Glorot initialization, He initialization - uniform distribution where range is specified according to network size

The aim of initialization is to not change the scale of the inputs when turning them into outputs (mean, std)
Can test the norm of the gradients as you train, they should not be changing through the network.

#### Mini Batching

Need to pad if the sentences are different lengths
Need to use masking

Can bucket sentences to have the same length in a batch
This can affect performance!

Batch size can matter.
SGD on single entries is very noisy, if you did it over every entry at once it would be smooth.
Minibatch is between between these.
The batch size alters the noise level.

Having noiser gradients at the start can result in a better start, but not a better end.
https://arxiv.org/abs/1711.00489 <- Don't Decay the Learning Rate, Increase the Batch Size

#### Learning rate decay

When valid performance starts to diverge, rewind back to last best point and optimize learning rate

### Debugging at Test Time

Training decoding disconnects
 - bug causing training to be good however actual performance in prediction to be broken
 - loss calculation is not the same between train and test
 - test may be over single elements instead of batches

Could have a bug with padding / masking
 - can find this by taking a batch, calculating loss, and then calculating loss over the individual entries and summing (or averaging as appropriate)
 - can create a unit test!

Your "decoding" (inference?) code should produce output that is consistent with the path that leads to the loss function.
You can call the loss function on the decoding output and test that the paths are the same.

### Beam Search

This is complex enough to have bugs!
Full search is NP hard, so must have heuristic.

Increasing the beam size should increase the performance (or at least maintain it).
Can test this by running with varying beam sizes and checking comparatively.
There are some graphs that cause the beam to fail?!?

Unit tests are good for finding this
Big difference between train and test set performance

### Look at the data!

Look at the outputs
 - is there an off by one error?
 - is the tokenization reversible / accurate?

### Qunatitative Analysis

Measure the gains quantitatively.
What is the focus? Is it getting better?

 - Are you focused on low frequency words? Are low frequency accuracies improving?
 - Are you focused on syntax? Is word ordering/syntax improving - e.g. long scale dependencies?
 - Are you focused on search? How many search errors?
 - etc

compare-mt <- might be good? In CMU repo
https://github.com/neulab/compare-mt

Having tools that allow you to do this analysis is very useful.

### Overfitting

Your model is not generalizing well.

You probably don't have bugs
Signs:

 - Good loss on training set
 - Good accuracy on training set
 - Bad loss on validation set

Maybe your model is not good!
 - Need a lot of domain knowledge to address this

Training loss converges well however validation loss diverges.
A large enough neural network can memorize the training data.

You can randomly assign labels to training inputs and the network will learn them!
Adaptive gradient optimizers overfit more on small data.

May not even need an entire epoch to overfit.
Don't need to randomly downsample - just reduce the number of epochs.
You can evaluate the model after a fraction of an epoch.

#### Early Stopping - Learning Rate Decay

These relate a lot to preventing overfitting.
newbob learning rate schedule

Stop training after the validation set stops improving
"patience" is a metric for how long to wait before stopping

Pick the stopping point according to the metric that matters
Learning rate decay on this is good too

#### Dropout

Regularization helps a lot with overfitting
Dropout consistently works

### Mismatch between loss function and actual evaluation

Maybe the model is getting more confident about it's mistakes?

If you are generating sentences is it the product of probabilities?
 - this would promote shorter sentences


