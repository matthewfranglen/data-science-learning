https://www.youtube.com/watch?v=W_x7BL-8VZc&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=14

CMU Neural Nets for NLP 2020 (15): Minimum Risk Training and Reinforcement Learning

### Maximum Likelihood Training

Predicting next word given previous words (or actions).

This is also called teacher forcing, as the history is always correct.
So the model only learns to work with correct history.
It also gives equal weight to wrong answers (very wrong is same as slightly wrong).

It would be better to be able to train with occasionally wrong history, and to vary the wrongness of different responses.

### Error

A better error metric would help with defining wrongness.
Can invert the "goodness" of the response (e.g. BLEU or METEOR).

Argmax is non differentiable, as it is discrete.
 - could use something like cross entropy?

### Risk

What is the expected error?
This is the risk.

The risk can be differentiated, however it is impossible to effectively calculate.
You cannot enumerate over all possible outputs.

#### Minimum Risk

This optimizes the risk.
The risk is calculated using sampling.

### Reinforcement Learning

Reinforcement learning is attempting to do the same thing as minimizing risk.
Reinforcement learning is where:

 * We have environment X
 * We can make actions A
 * We then get a delayed reward of R

Example: Playing pong - X is the observed image, A is moving the paddle up or down, R is the win or loss of the game

#### Problems

Reinforcement learning is unstable.
If the action space is very large it can be very hard to train.

#### Why use this for NLP

The "reward" of NLP is a dialog with a human.
The reward comes at the end.

This can be used for Q&A where the selection of the context comes first, and then the context is used to answer the question.

#### Self Training

Can just use the current output of the model and treat that as the correct answer.
The problem with this is obvious - the errors will just be reenforced.

Can use two models and only train when they agree on the output.
Distillation training (a large model as "teacher" trains a smaller model) also works like this.
Distillation can also train from an ensemble to a single model.

Can make the input noisy, as the output is also noisy.
That can make the model less reliant on the input being perfect.

### How do we know what action lead to the reward?

The best training scenario is that there is immediate reward, so the last action is directly tied to the reward.
The worst scenario is that the reward is only at the very end.

### Can scale the reward according to the expected performance

We expect to perform better on easier tasks, so the reward for them should be lower.
Can predict the final reward based on a linear regression from the current state (can be for whole sentence or single word at a time).
Or could use the mean of the rewards in the batch.
Can actually train a model to try to predict the reward too heh.

With this approach you are essentially trying to beat your current performance.
You can still learn even if you only get negative rewards.

### Increasing Batch Size

This can improve stability.
Each sample has high variance, so by increasing the batch size we are sampling more values before updating.

### Warm Start

Can start using one more easily trained dataset (e.g. using maximum likelihood) and then switch to reinforcement.
Can even gradient the switch over a number of batches / epochs.

### Reward vs Variation

Reward based learning can just end up causing the same action to be picked all the time.
It can be good to vary the choice at points.

 * Can randomly select an output sometimes
 * Can reward new outputs that have not been done before
