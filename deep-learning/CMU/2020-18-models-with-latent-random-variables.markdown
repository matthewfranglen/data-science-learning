https://www.youtube.com/watch?v=5OL1_YECHvM&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=1://www.youtube.com/watch?v=5OL1_YECHvM&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=17

CMU Neural Nets for NLP 2020 (18): Models w/ Latent Random Variables

## Discriminative vs Generative Models

A discriminative model calculates the probability of an output given input P(Y|X).

A generative model calculates the probability of a variable P(X), or multiple variables P(X,Y).

## Types of Variable

Observed vs Latent

Observed - something that we can see in the data
Latent - something we assume exists but cannot directly see in the data

Deterministic vs Random

The output of a language model is random because it forms a distribution that you could sample from

## Latent Variable Models

A latent variable model is a probability distribution over two sets of variables (x and z).
The first variable (x) is observed at learning time, and the second (z) are latent variables.

## Latent Random Variable Model

Older latent variable models.
Unsupervised topic model
Hidden markov model
Tree structured model

## Why use a Latent Variable Model?

Some variables are not observed natually and we want to be able to infer or model them.
This can be things like the topic of an article.

Can also speciy the structural relationships in the context of these unknown variables to learn structure.
Things like POS taggers could be this.

## Deep Structured Latent Variable Models

These specify a structure.
Used for things like POS tags.
This specification can limit the flexibility.

### Examples

 * Generative Adversarial Network
 * Flow based generative models
 * Variational Autoencoders

## Variational Autoencoders

We observed x
We have a latent variable z generated from a gaussian distribution
We have a parameterized function that maps from z to x, this function is usually a neural network

So this is creating a function to generate values in x from "random" inputs from z
This is really learning the data distribution of x

### Loss Function?

Ideally would maximize the log likelihood of the entire corpus of x
This is difficult though, the single values that are produced are hard to work with.

Could sample z values and then sum the different values.
However this is inefficient as you have to draw a lot of values from z.

This ends up being an operation to minimize the Kullback-Leibler divergence between the approximate distribution and the true distribution.
This still has the problem of the true distribution being in the equation.
We can calculate a lower bound on this value, and then optimise that.
