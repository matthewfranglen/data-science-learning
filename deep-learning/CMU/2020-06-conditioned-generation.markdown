CMU Neural Nets for NLP 2020 (6): Conditioned Generation
https://www.youtube.com/watch?v=Og3_LngQE_4

Conditioned Generation
----------------------

Starts with an example of training a language model on Harry Potter books and then using that to generate some (poor) text.
The idea here is that a language model, by itself, is a generative model.

### Conditioned Language Model

This generates text according to a specification.
The input does not have to be text.

This could be things like structured data, describing images, translating text between languages etc.
There are companies that revolve around this kind of generation.

#### Encoder Decoder

Use the initial part of the model to read the input text, passing it through the model and generating a final state.
(the model here might be chained LSTM for example)
This is the encoder.

Then use this final state as the starting state for the model which generates the output.
This is the decoder.
If the decoder model is recursive, like an LSTM, then it will take the previously generated word as part of the input when generating the next word.

Can use a model (simple) between encoder and decoder - a transformer was suggested for resizing.

#### Generation

The generation of sentences given probability distributions is not entirely straightforward.
Should the highest probability word be used at every point?
What about the total output probability?

 * Sampling - choose word randomly according to the multi bernoulli distribution represented by the probabilities
 * Arg max - choose the total output that maximizes the probability across all words

(substitute words for tokens as appropriate)

These search problems may choose a set of output that the model believes is good.
However is the _model_ good?
The search algorithm does not address this.

##### Ancestral Sampling

Just choose each word according to the probability distribution.
The simplest possible implementation.

##### Greedy Search

There is no efficient search algorithm to satisfy the argmax approach as the number of potential sentences to evaluate is much too large.
Executing the model is a non trivial cost too.

The greedy search will take the highest probability word at each choice.
This will fail though.
It falls into a local optimum.

##### Beam Search

Maintain several paths through the choices and choose the best one.
This might maintain a history of 3+ tokens that were chosen.
(Can reduce it to a fixed count at each round, even if that does not completely collapse the depth - so the length of the beam can vary based on the generation).

The probabilities are composed through multiplication so longer sentences will have a lower score.
Can try to convert this to a score per word to counter this.
An empty string can frequently have the highest probability.

The beam hyperparameters are covered in a paper that is on the reading list.
A suggestion of 5 should be better than using 1.

### Model Ensembling

Combining the predictions of multiple models.
Different models make different errors and the errors should be uncorrelated.

Ensembling works almost all the time.

Models do not have to be that different, could even be same architecture with different initial parameters.
The more decorrelated the errors, the better.

#### Linear Interpolation

Combine the probabilities from each model with a product.

If the model makes uncertain errors then the other models should override it.
If the model makes infrequent errors then the other models will tend to override it.

Can trust different models unequally, need a good way to come up with the ratios.
The ratios need to maintain the probability distribution.

#### Log linear Interpolation

Sum the logs of the probabilities for each model-class output.
Can use a weighting coefficient per model (does not have a restricted range, as the results are renormalized).

Need to renormalize afterwards (e.g. softmax)
This is because now that the log values are being combined the probability distribution will no longer be well formed (all between 0 and 1, sum to 1).

#### Which to use?

Linear = logical OR
  Any choice that a model gives a high probability to (multiplying the probabilities tends to suppress those that have low values)
  Needed if models can assign zero probability.

Log Linear = logical AND
  Chooses things where all models agree
  Needed if you want to restrict the possible answers
  (log 0 = -inf, so it would break the linear interpolation)

#### Downsides

Slow, take a long time to train.

Can train the ensemble and then distil the knowledge into another model (train a single student from the ensemble).

 * Try to make the student model "match the description over predicted words" (Graham words, not sure of intent)
    - explained: you are training against the probability distribution, not the highest choice
    - essentially trying to match the output of the ensemble _exactly_ instead of producing the same final result
    - this can reflect the confusion that the ensemble has, preventing student learning certainty that doesn't exist
    - can also use this on unlabelled data
 * The desire is to have the distilled model make the same kinds of errors as the ensemble

This has been shown to increase accuracy notably.

Can be tricky to train language models (or general sequence models) like this because of the unpredictability of the next word from the model.
Can take sequences from the teacher model and then work as if the student had predicted them at each step.

#### Parameter Averaging

If you are training the same model N times for the ensemble then you can periodically average the models together.
If you are training different models then you can checkpoint each model several times near the end of training and then average the model with it's historical checkpoints.

The correlation of the errors between models will be more significant here, the batch shuffling should help.
The different checkpoints will have seen different things most recently.

#### Stacking

Use one model to predict, then feed those predictions to another model.

### Evaluation

Parallel test set - then use system to process test entries and compare results to correct results.

Can use humans to generate the results.
Can rank different possible labels for a given entry.

If you have a production model then you can often use the observed outcomes (purchase, dropout) as labels.

#### BLEU

Compare the n-gram overlap with the target output.
Captures partially correct outputs.

This can fail to match human evaluation of the output.
The dimensionality of the available output space also limits the quality of this evaluation approach.

#### METEOR

This is like BLEU however considers things like paraphrasing or reordering or function word vs content word differences.
This does require extra resources for new languages.

#### BERTScore

You can compare the embeddings of bert for the different outputs!
LOL.

The idea is that bert can capture the semantic similarity.
This is quite an opaque means of judgement.
Having an opaque means of evaluation is a very tricky proposal.

#### Perplexity

This is an evaluation of the probability distribution directly.
This should be supplemented with another evaluation approach.
This can help for ambiguous outputs.

Perplexity doesn't actually measure the quality of the overall output!

Unconditioned Generation
------------------------

How to evaluate unconditioned generation models?
(remember conditioning being generation according to a specification)

A language model can be this (e.g. a book writer model).
If the language model just memorizes the input data then it will produce high quality output however it adds nothing.

Can you evaluate on a held out set?
Can you make it conditioned generation - like by adding a prefix and then generating subsequent words?
