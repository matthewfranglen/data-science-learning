https://www.youtube.com/watch?v=9OA8IybwI00&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=13

CMU Neural Nets for NLP 2020 (14): Search-based Structured Prediction

## Structured Prediction

There are many varieties of structured prediction.

## Normalization

Is the model normalized?
Locally or Globally?
(normalized = probabilities sum to one).

It can be very hard to normalize globally.

### Approximating Normalization

Can use sampling to estimate the probability distribution.
 - As number of samples grows large the distribution will approach the true distribution.
 - However it can have high variance (could sample rare things)

Beam search.
 - Use k best hypotheses
 - However this has a bias based on the size of the beam (things have to look good partially)

## Unnormalized Models

Normalization is not necessary
Most of the time we just want the best option

Why would we want normalization?
  Perhaps to give a confidence level
  Perhaps to feed the results into a downstream model

Calibration vs Performance
  Calibration is "if I say 30% chance then it occurs 30% of the time"
  Performance is "the highest confidence thing is the most likely"
  Neural models tend to perform well but be poorly calibrated

## Training an Unnormalized Model

Take the best output, if it is wrong then alter the model to promote the actual result.

### Hinge Loss

Will it fit to the best separation of outputs?
Can use a hinge loss to penalize answers within a certain margin.

This is done by calculating a loss that has a positive value _even if_ the highest value is correct.
Instead the highest value must have a certain margin above all other values to be considered completely correct.
This is called a hinge loss.
This can result in worse results!
This does make the classifier more confident all the time.

The hinge loss can be specialized as there are some decisions which are worse than others (false positive diagnosis of cancer vs false negative).
By allowing variations you can force a greater confidence for certain choices.
For sequences having every element be wrong is worse than only one element being wrong, so we can penalize according to the number of incorrect elements.

When training a language model, you can add N to every answer _except_ the correct one, and then see what the argmax is.
If the top value is wrong then the correct answer is not sufficiently strong.
This can help with models where the previous predictions must be right (like a language model).
