CMU Neural Nets for NLP 2020 (5): Efficiency Tricks for Neural Nets
https://www.youtube.com/watch?v=eokkF3qv8_U&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=5

CMU Efficiency Tricks for Neural Nets
-------------------------------------

Neural networks are slow - what can we do about it?
I should check out tensorboard too.

GPUs love numerous operations but don't like "big" operations.
Optimised implementation of batching.
Parallelism is really good on a GPU.

Can use approximate operations when the problem is too big to be tractable.

### GPU Training Tricks

CPU is like a motorcycle, or a really fast runner.
    They can accelerate fast, top speed not too bad.
    Very versatile.

GPU is like an aeroplane.
    Takes forever to start, gets extremely fast when running.

E.G. Matrix multiplication - only gets faster on GPU when the matrix is >128x128 (quite big).
    CPU 20x faster at 16x16
    1x at 128
    GPU ~100x faster at 2048x2048

CPU does not parallelize these tasks as well as a GPU does - locks, shared mem etc.

CPU can be used for prototyping or for unit tests.
    E.G. some very small matrices.

Sometimes the operation cannot be expressed in a way that makes it efficient on a GPU.

Test the speed of GPU vs CPU and then work out the cost.
    Can you get more CPU cores to make it as fast for the same price?

### Optimising things

Don't repeat operations, pre calculated and then use the pre calculated values.
Attention can be amenable to this optimisation.

Reduce the number of operations.
Can multiple operations be compressed together?
pytorch-struct has lots of operations that can be compressed for common algorithms.

Reduce the movement of data between GPU and CPU.
Can be difficult to track.
Try to move memory as early as possible.
Input data is particularly eligible for this.

Most GPUs only have up to 12G so memory is a major issue.
Minibatch size matters for this.
    Remember you can resize the batch and run backprop less frequently to act as if you had a larger batch.
    Minibatches make things faster, however they ALSO provide more training stability.
    The space allocation for accumulating gradients is always present, so repeating the batches before applying the backprop does not cause a memory explosion.

What takes up the most memory?
    For very large models - parameters can be the largest. Remember that adam has 2 values per parameter.
    For smaller models - either the hidden states or the softmax.
    Look for a memory profiler, there should be one available.

Batch norm is not used that much for NLP
    Instead layer norm is used!
    Batch norm can affect the approach of repeated minibatches.

### Parallelism

 * Within-operation parallelism
     For example matrix multiplication

 * Operation-wise parallelism
     a(x) + b(x) can be split so that a(x) is calculated on one thread, and b(x) on another
     Can be difficult to coordinate the memory flow

Both of these can be optimised even if you have a batch size of 1

 * Example-wise parallelism
     Process each training example in a different thread or different machine
     Need to merge the weight updates, need to share the parameter updates
     Pytorch DistributedDataParallel
     What happens if you have a slow machine?
         It is possible to do this asynchronously
         Look up hogwild stochastic gradient descent - dispatch jobs, collect updates, apply immediately, yolo

### Negative Sampling

If you have a really large vocab, e.g. entire english language.
Too many to calculate every time.
    Want to discriminate the negative samples from the positive one.

Can sample the negative samples to reduce the number of things to update.
This seems a bit like the probabilistic masks I did.
    Importance sampling - reweights the probability of choosing a sample based on the prevalence of the entry
    This is a lot like what I did!
    Could be a biased estimator (especially when the count of samples is small)
        Can incorporate the actual output of the sample - if it is high then it needs correcting (as it is not the true output).
    Used in word2vec - samples k negative samples
    Can use the same negative samples across the minibatch.

Hard negative mining - look up

### More efficient Predictors

Can make softmax more efficient to calculate
    Two level hierarchy, predict class first then actual word (so two smaller sets which happen to cover the whole thing)
    Optimal improvement is O(N) -> O(2 M^1/2)
    Need to train the class discriminator first!

Could use a deeper tree.
    Need to do each prediction in serial though?

Maybe you could predict the tree up front.
    The problem is that the output jumps around the word space _very_ quickly.

Could predict an embedding and find the nearest neighbour to work out what you are predicting.
This is like clustering the words.
    Needed special loss function (Von-Mises Fisher distribution loss, minimizes cosine similarity)
    The loss is really an angle and will likely end up normalized to 1 otherwise
    With high dimensions everything is spread out
