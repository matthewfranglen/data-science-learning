https://www.youtube.com/watch?v=ry-__6gNSqE&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=10
CMU Neural Nets for NLP 2020 (11): Structured Prediction with Local Independence Assumptions

(the title on the slides is actually structured prediction with local dependencies)

### Types of Prediction

There are lots

 * binary/multi-class classification (what class is it)
 * exponential or infinite label classification (structured prediction)
    - e.g. parts of speech tagging
    - e.g. translation

#### Why is it called structured prediction

Structure is trees or graphs.
The classes are too numerous to enumerate, so you can't just calculate loss for all of them.
You need a structure for the output space to be able to make the problem tractable.

### Varieties of Structured Prediction

#### Models

 * RNN
 * Self attention / convolution
 * Conditional random fields with local factors
    - will cover today

#### Training algorithms

 * Maximum likelihood with teacher forcing
    - Was covered already, teacher forcing is where the teacher provides the "last state" to predict from
 * Sequence level likelihood with dynamic programs
    - will cover today
 * Reinforcement learning / minimum risk training
 * Structured perception, structured large margin
 * Sampling corruptions of data

#### Example Problem

Sequence labelling, e.g. part of speech tagging (or named entity recognition).
Span labelling, e.g. named entity recognition

#### Why do this?

Consistency in the output with the prediction is important.
Just averaging over the individual values that are possible would make nonsense output.
Need to structure the averaging so the model is hinted towards the right answer.

#### What does this look like?

Something like a BiLSTM doesn't really structure the output? (yet they claimed that a biRNN does? I don't understand)

So if you feed the _predicted pos_ into the next stage of the RNN that becomes a structured prediction model.

#### What problems does this solve?

Fully independent classification
 * Based on strong independence assumptions about the different classifications performed
 * Thus there is no guarantee of a consistent structure across the different classifications

History based or sequence to sequence
 * No independence assumption
 * Cannot exactly calculate the classification of any element - need things like BEAM search
 * Makes training more tricky
 * Exposure bias

### Teacher forcing and exposure bias

Teacher forcing: system is trained receiving only the correct inputs during training
 * When calculating the probability of the next word, you base it on the _correct_ history

Exposure bias: at prediction time you are working off your own predictions
 * These can be wrong now!
 * This can compound errors as the model over relies on the previous predictions
 * (called exposure bias because it was never exposed to this bias)

#### Local Dependencies

Some independence assumptions on the output space, not entirely independent (just local dependencies)
Can decode/train via dynamic programs

Conditional random fields are an example of this

##### Local Normalization vs Global Normalization

local normalization = each decision made has a probability that sums to one
  - easy to train, can test it at each point easily
  - vulnerable to exposure bias though

global normalization = each sequence has a score where a single decision probability does not sum to one
  - it's now relative over the entire set of sequences, still may not sum to one however you are comparing at the right scale
  - you can't really calculate the total set of probabilities due to the size of the space

### Conditional Random Field

It's a set of connections between all the inputs?
First order linear CRF only connects neighbours so it is still feasible to enumerate the connections.
  - you then train those connections?
  - they represent the dependencies between the nodes
The general form is everything is connected to everything else.

can convert local to general, but unlikely to go the other way
(local is a subset of general after all)

#### How do you actually train it?

Lots of complex math

The core point is that we can calculate it recursively.
Only need to know the sequences ending in the last word, then can calculate the next step.

These scores are not probabilities, instead it's a log space to determine the things that are more or less likely.

Apparently this is quite straightforward to implement, even if the math that was used is a bit much.
Might be good to review it.
https://github.com/kmkurn/pytorch-crf found this for pytorch, might be worth looking at.

https://github.com/harvardnlp/pytorch-struct
