CMU Neural Nets for NLP 2020 (9): Sentence and Contextual Word Representations
https://www.youtube.com/watch?v=EjoTMiZPVC8&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=8

## How to represent contextualized words or sentences

What tasks require sentence representations?

 * Sentence classification (e.g. sentiment)
 * Paraphrase identification (summarization, equivalence)
    - exactly the same is too restrictive, just want it to be similar enough
 * Semantic similarity/relatedness
    - quantify how similar two sentences are
    - how does the negation of something relate to the original
 * Textual Entailment
    - Entailment: if A is true then B is true
    - Contradiction: if A is true then B is not true
    - Logical reasoning?
    - closed world system (anything not stated is false)
    - Neutral: A does not relate to B
    - Entailment is directional, paraphrases entail in both directions
 * Retrieval

Turning a sentence into a single vector can help with all of these.

### Representations

Things like ELMo / BERT generally refer to:

 * Model
 * Training Objective
 * AND Data

So it can be hard to compare them

#### Contextualized Word Representations

Vector per word
context2vec

Can do bi directional LSTM to then predict the word
Like word2vec except instead of cbow it's lstm either side
  - problem is the disconnection between the two sides, needed to actually have a task

## Models for Sentence Pair Processing

 * Calculate vector representations
    - how do you get this representation?
 * Feed those into a classifier
    - for two sentence vectors, S1 and S2 you can feed in {S1, S2, S1 * S2, S1 - S2}
    - multiplication promotes sameness
    - subtraction promotes difference

## Tasks Datasets and Methods

There are lots of tasks, and each one requires different data.

 * Only text - language modelling
 * Naturally occurring data - e.g. machine translation, EU documents (translated), wikipedia
 * Hand labelled data - expensive!

What can't you learn from a language model?
  - Long term relationships?
    - Very rare things that affect the prediction will generally be ignored
  - Language models are generally bad at predicting facts
    - The general text of the language rarely encodes facts
Language models take short cuts, won't encode things that arn't required for reasonable predictions.

Skip Thought
  - Predict the preceding and following sentence based on the current sentence
  - Like word2vec except sentence level
  - Maybe I could use this in the workshop?

paraphrase.org - paraphrase database!
  - can do hard negative on this

RoBERTa
  - same as bert but trained longer
  - apparently _always_ better than bert

### Which Model works best?

The answer is not currently clear

## Training Objectives

Often the dataset used to train is chasing leaderboards
  - For a custom domain you probably have to train it yourself

### Multitask / Transfer Learning

Multi Task learning - general term for training on multiple tasks
  - Can increase the data available
  - High resource language -> low research language
  - Related tasks perform better

Transfer learning - type of multi task learning where we only care about one of them
  - e.g. language modelling -> language classification

Pre-training
  - first train on one task, then on another
  - this can help if the first task has a lot more data available
  - Multi task learning is always better, however it has data requirements
  - Can produce much better results than directly training on the target
    - Can keep the data the same for the pretraining and the final training
    - The representation learned for the language model is much stronger than just a yes/no classifier

Domain adaptation - type of transfer learning, where output is the same but the domain is different (e.g. general language model -> medical language model)

 * Maked word prediction
    - predict a word given context
    - 80% of the time the word is replaced with MASK
    - 10% of the time the word is randomized
    - 10% of the time the word is _unchanged_
  The randomization means this works to counteract noise in the dataset

