https://www.youtube.com/watch?v=8f_IzoafNgc&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=12

CMU Neural Nets for NLP 2020 (13): Generating Trees Incrementally

This will cover dynamic programming models, the last one was about transition based models.

### Models to calculate phrase structure

Parsing is similar to tagging

Tagging is search in a graph for the best path

Parsing is search in a hyper-graph for the best tree

#### What is a hyper-graph

A degree of an edge is the number of children of that edge

 * degree 1 - single child (like a regular edge)
 * degree 2 - two children for single edge parent
 * degree 3 - three children...

The degree of a hypergraph is the maximum degree of it's edges
A graph is a hypergraph of degree 1

A hypergraph is like the possible ways to parse the structure of a sentence.

#### Weighted Hyper Graph

Just like a weighted graph, each edge has a weight.
By making the weights probabilities you can then search for the best graph in the hypergraph.

e.g. CKY Algorithm
this uses a hypergraph search

### Why care?

Hypergraphs free you from thinking about the accumulation of a single graph, and instead move to a search through the hypergraph.
What does this allow you to do?

You can create multiple low level elements of the graph (like different translations of small phrases)
then try different combinations of them

### How do to this as a model?

Neural CRF Parsing
(2015 - Durrett and Klein)

#### Even Simpler Model

Decide whether a span is part of the sentence.
top down approach

can predict if it is a constituent of the current phrase

can start with the whole sentence, then try to split in two, etc etc

### Does the word actually matter?

The interaction between the specific word and the structure of the sentence is both present and subtle

I saw a girl with a telescope
 - does the girl have the telescope? unknown

I saw a girl with a skateboard
 - does the girl have the skateboard? yes

## Dependency Parsing with Dynamic Programs

(not just limited to dependencies)

Express sentence as fully connected directed graph.
Score each edge independently.
Find maximal spanning tree
