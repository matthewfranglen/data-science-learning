https://www.youtube.com/watch?v=6_gMeW_cunQ&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=19

CMU Neural Nets for NLP 2020 (20): Multitask and Multilingual Learning

Multitask Learning is effective in NN.
This is because NN are feature extractors.
Multitask learning is about extracting features that are good across multiple tasks.

Embeddings are how the features are extracted.
The embeddings can be used in multiple contexts.

## Types of Learning

Multi Task learning - training on multiple tasks
Transfer learning - type of multi task learning where you only really care about one of the tasks
Domain adaptation - a type of transfer learning where the output is the same but want to vary the topics or genres etc

### Standard Multi Task learning

Train representations to do well on multiple tasks at once.
It can be done as simply as randomly choosing a minibatch from one of several tasks.

### Pre Training

Train on one task with lots of data, then train on another.

This is used a lot - like training a language model then fine tuning.

Moving from one task to another typically leads to forgetting how to do the previous task.
(catestrophic forgetting)

#### Regularization

The adaptation to the second task that has a small dataset can result in just memorizing that small dataset.
Regularization can prevent this.

Early stopping - stop training when the model starts to overfit
Appropriate learning rate matters too.
It may be appropriate to continue after the validation starts to increase.
The other option is to checkpoint regularly and then rewind, restarting with a lower learning rate.

These are a bit indirect though.

L2 regularization based on the divergence from the initial parameters.
This pushes the model back to the initial parameters.
This does require keeping the initial parameters though, which can be expensive.

Dropout - it's great heh.
Can be just as good as the L2 regularization.

#### Selective Parameter Adaptation

You can retrain some of the parameters (instead of all of them) when fine tuning and this can lead to better performance.
Can choose to adapt some of the parameters at a time instead of all at once.

The parameters to choose are task specific.
Think of what has not yet been learned well.

#### Soft Parameter Tying

Can share the parameters between tasks loosely.
They arn't strictly shared, but they do regularize to become closer to each other.

#### Different Layers for Different Tasks

Some tasks are easier to learn than others (e.g. POS tagging).
Can have separate layers which perform each of these tasks, and you can then train them to perform that specific role.

Can choose to get the output early if you only need a specific output.

This can happen implicitly when training large models - e.g. BERT has a layer that does POS tagging quite well.

### Domain Adaptation

Have one task, but the data may come from specific distributions.
E.g. News text vs Social media posts.

A model trained on one distribution may not do well on another.

Can actually append a tag to the input to allow the model to distinguish and treat the distribution of the input differently.

### Multi Lingual Learning

Languages are often similar which means you can learn common approaches for several.
It can also save memory to have a single model that can handle multiple languages.

There is an upper limit to the capacity of the model to handle multiple languages.

#### Data Balancing

Can tail off the number of samples from high resource languages to allow rare resource languages to train.
This is done so that the distribution of inputs at the start represents the size of the respective datasets, and as the training progresses more common languages become less likely to be picked.
