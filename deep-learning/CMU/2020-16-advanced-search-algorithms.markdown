https://www.youtube.com/watch?v=mfOCPBOHVjY&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=1://www.youtube.com/watch?v=mfOCPBOHVjY&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=15

CMU Neural Nets for NLP 2020 (16): Advanced Search Algorithms

# The Generation Problem

We have a probability model, how can we use it to generate a sentence.

## Sampling

Sample according to probability distribution.

### Ancestral Sampling

Randomly generate words one by one, until reaching end of sentence.
This exactly samples from the distribution.

This is sampling with replacement - can sample same sentence twice.

## Argmax

Take the strongest probability.

## Best?

The best possible single output involves searching, so neither sampling or argmax is appropriate.
We want to observe multiple outputs, and we want them to be diverse.

When searching can force things to be different.

### Why Search?

Want the best output.
What is the best though?

The most accurate output has the lowest error, however how could we find that?

Can search for the most probable output.
Most probable does not mean most accurate.

The lowest Bayes risk (the output with the lowest expected error).

### Errors

Search error is where a model fails to find an output that optimizes the criterion.

Model error is where the output optimises the criterion but does not optimize accuracy.

The error of the model will go down (training does this) however that may not increase accuracy.

## Searching Probable Outputs

### Greedy Search

Just pick the highest probability word (argmax repeated).
Will generate the easy words first, will prefer common words to rarer words.

### Beam Search

Maintain N multiple paths and expand each one, then take the next set of N paths from most probable expanded paths.
Have to expand N * vocab paths.

## Pruning

### Histogram Pruning

Keep N hypotheses at each time step.
This will always have the same load, due to retaining the same number of things.

#### What Beam Size?

Larger will be slower.
However larger may not be better due to model errors.

Can work it out empirically.

### Score Threshold Pruning

Keep all hypotheses where score is within a threshold of best score.
This can explode, keeping a lot of different options.

### Probability Mass Pruning

Keep all hypotheses up until a total probability mass is reached.
This can explode, keeping a lot of different options.


Can combine these options to limit the total number of hypotheses that are retained at any point.

## Problems

The model can push difficult options to the end, especially rare outputs which may not've been trained enough.

### Future Cost

Don't just predict how hard it is right now.
Predict how hard the whole sentence will be.

This is a little like the transition from path-length-so-far search to a* search.
Need to be able to estimate the cost to the goal.
This isn't exactly like a* as the cost estimate could be an underestimate.

### Better Search can lead to errors

This is due to errors in the model.
Need to train the model to better correlate the model score with accuracy.

Can change the decision rule to minimize the risk
Can heuristically modify the score afterwards
Can randomly introduce errors, but errors that are like the errors you want


