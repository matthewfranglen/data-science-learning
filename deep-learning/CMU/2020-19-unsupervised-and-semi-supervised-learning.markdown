https://www.youtube.com/watch?v=rpAzfgr3OGc&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=1://www.youtube.com/watch?v=rpAzfgr3OGc&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=18

CMU Neural Nets for NLP 2020 (19): Unsupervised and Semi-supervised Learning of Structure

## Supervised / Unsupervised / Semi-supervised

Supervised is the most common training regimen.
Model calculates P(Y|X) (probability of Y given X) at training time, given both X and Y.

Sometimes unsupervised is given.
Model calculates P(Y|X) at training time, given only X.

Semi-supervised can give either X or X and Y.

Unsupervised training is done for all state of the art systems now.

### When training BERT is it supervised?

This is feeding it a bunch of text to generate a language model (is it?).
Bert is not intended to be well calibrated.

Bert is bidirectional so it can be used to predict masked out words.
Predicting masked out words is supervised, as it is given the masked out word as a training target.
The unsupervised part of training bert is the latent features of the language.

### Learning Features or Structure?

Features are things like word embeddings.
Structure is things like a POS tag graph.

The discrete structure can allow model information to flow differently.
It can also be more interpretable.

### Unsupervised Feature learning

When learning embeddings we have an objective.
The intermediate states are the unsupervised part.

Things like cbow / skip-gram / sentence-level auto-encoder / skip-thought vectors / varational auto-encoder

The embeddings can be used to solve tasks, however they are often used to initialize downstream models.

## What is the Objective

A generative model of the data.
Can be as a traditional generative mode, or an auto-encoder, or a variational auto-encoder.
