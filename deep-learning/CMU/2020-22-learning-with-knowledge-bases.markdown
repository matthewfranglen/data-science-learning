https://www.youtube.com/watch?v=Lcb5YKE21P8&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=21

CMU Neural Nets for NLP 2020 (22): Neural Nets + Knowledge Bases

## Knowlege Bases

Structured databases of knowledge containing entities and relations.
Can express this as a graph (entities as nodes, relations as edges).

How can we learn to create or expand them using neural networks?
How can we answer questions with knowledge bases?

### Word Net (1995)

Large database of words including parts of speech and semantic relations.
  e.g. is-a has-a part-of antonymy

Only common words.

### Cyc (1995)

Manually curated database attempting to contain all common sense knowledge.
The knowledge in this database is hard to incorporate.

### DBPedia (2007)

The structured data in wikipedia.
Ontology available.

Anything you can find in wikipedia.

### Freebase / wikidata (2008)

Curated database of entities that are linked and extremely large scale.
"the data version of wikipedia"

Anything you can find in wikipedia.
Lots of missing information - 71% of humans were missing date of birth.
~300GB

## Learning Knowledge Graph Embeddings

Express triples as additive transformation (vector summation)
Can use TransE to do this (Bordes et al 2013)

The distance between the triple and the actual entity should be minimized if the relationship exists.
It should be maximized if the relationship does not exist.

There are problems with many-to-many relationships as the minimization of distance would bring potentially unrelated entities together.
Handling this is tricky and is a current area of research.

### Decomposable Relation Model

Can express relationships as a vector that is split into concepts.
The relation map from entity to entity is then the entity coordinates are mapped through the concept vector.

This can lead to interpretable concepts in the relations.

Is this just the product of the relationship vs the addition (for the previous one-to-one approach)?
The term attention was used quite a bit so this could be an attention mapping over the entity vectors.

### How to Complete Knowledge Bases?

Can create a NN to predict if a relationship exists.
This operates over the graph.

Can then try to extract relationships from text.

Can repeatedly apply a trained model to text to extract further training data.
Finding entities and then searching for all text that contains both entities to determine relationship.
This creates noisy labelled text.

## Injecting Knowledge into Language Models

Provide LMs with topical knowledge
Aim to make the generated text more factually correct

Can use copying from the source as well as the topic database
Can have the model generate special tokens which represent things like "entity name" or "entity birth date"

## Reasoning over text corpus as a knowledge base

The text needs to be transformed into a knowledge base (entities and relationships).
Need to be able to operate over the mentions of an entity.

OpenIE
