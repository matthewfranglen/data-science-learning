CMU Neural Nets for NLP 2020 (7): Attention
https://www.youtube.com/watch?v=jDaJYOmF2iQ&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=6

Attention
---------

How do you represent a sentence?
> You can't cram the meaning of a whole sentence into a single vector - Ray Mooney

What if we could use multiple vectors based on the length of the sentence?

This is attention

encode words into vectors
decode into linear combination of the vectors weighted by attention weights

query vector (last state of input) key vector (encoder states)
combine the query vector with each key vector to make the value vectors
Then you can combine these to make a single overall vector
Then softmax afterwards

softmax picks a single word so it tends to focus the output for the current task

Original paper used multi layer perceptron, can handle any problem, however it's a bit indirect
More direct methods of comparison can allow the network to learn to solve the problem more directly
Bilinear allows you to map the input vector to a desired shape and then do a dot product (which is a direct comparison between vectors)

The dot product approach needs scaling to ensure that the length of the vector does not impact the value

batched_attention.py

This uses NLP attention - what would be required to make it use a different kind of attention?

### What do we Attend To?

Input Sentence: Copy
  Summarization
  Can have problems copying things if not seen in the training set

  Can generate output in two ways - one from the normal path (embedding, various layers), one from attending to the input sentence
  Can be useful for text generation, e.g. copying the names of players in a game

Input Sentence: Bias
  For translation you can use a dictionary of equivalent terms to bias the output probabilities
  Rare words can benefit from this particularly - e.g. location names

Previously Generated Things
  like the previously generated words in the sentence
  Language is bursty - if you say something, you are likely to say it again
  So rare names you say are good to attend to as you are likely to repeat them

  This can just repeat errors though!

Various Modalities (?)
  Can attend to other things like images or speech
  This is like the conditional generation?
  Can do multi modal attention, e.g. sight and sound (speech?)

Hierarchical Structures
  encode with attention over words first, then have another attention over sentences on that

Multiple Sources
  Can attend to multiple related information sources
  e.g. sentences in german AND french for translation to english
  This could be useful if multi lingual text is available - subtitles, official documents in EU

Self Attention (aka Intra-Attention)
  each part of a sentence can attend to other parts of the sentence
  e.g. determine type of bank by attending to money in the same sentence

  This can obviate the need for a RNN as it is essentially recursive
  Bad at counting the number of words in a sentence

### Improvements to Attention

Coverage
  neural networks tend to drop or repeat content
  can model the number of times a word has been covered
  can impose a penalty if the attention is not approximately 1 over each word
  can add embeddings indicating coverage?

Incorporating Markov Properties
  Attention last time tends to be correlated with current attention
  Want to avoid big jumps of attention
  Can add information about the last attention when calculating current one

Bidirectional Training
  Attention should be roughly similar in both forward and backward directions
  Can make training promote this quality

### Supervised Training

Can establish alignments in the training set
  Like manual alignment
  Pre train with them, get the model to match the alignments

  Attention is often not alignment
  Can be blurred across words, or off by one
  The important thing is related words, not the direct word, which is why it could be off

  Attention attends to the hidden state, so the hidden state is for the next word

### Specialized Attention Varieties

Hard Attention
  instead of soft interpolation - make a 0/1 decision about attention
  hard to train a discrete set of values
  This is better for interpretability
  Rarely used

Monotonic Attention
  Output might be in same order as input
  Speech recognition
  translation
  summarization?

Multi-headed Attention
  Multiple focuses on different parts of sentence
  e.g. copy vs regular generation
  can be multiple independently learned attention (like in the transformer model which I read about)
  one head for every hidden node (this is silly)

### Attention is all you Need

Introduction to transformers
Understanding the different approaches ensures that you do not just focus on transformers
No RNNs used

encoder -> self attention
decoder -> previous generated words attention
lots of self attention, multi headed attention
normalized dot product attention (hidden state of decoder and hidden state of encoder, multiplied by the key matrix - bilinear?)

positional encodings
  This covers the functionality that a RNN would provide (e.g. spotting difference between the same word at different points in sentence)
  a constant period vector (e.g. Sinusoidal vector) is used to encode distance

### Training Tricks

Layer Normalization
  layers remain in reasonable range (batch norm? but on each element in the layer. batch norm is dependent on batch size)

Specialized Training Schedule
  Slow start

Label Smoothing
  Add some uncertainty

Masking for efficient training
  ?
  Perform training in as few operations as possible using big matrix multiplies
  Can mask results for the output
  (I should clarify this further)
