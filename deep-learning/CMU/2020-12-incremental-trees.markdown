https://www.youtube.com/watch?v=-bG-QfVrsYw&list=PL8PYTP1V4I8CJ7nMxMC8aXv8WqKYwj-aJ&index=11
CMU Neural Nets for NLP 2020 (12): Generating Trees Incrementally

Syntax Trees vs Semantic Trees, generation of them during parsing

### Kinds of Parsing

Dependency Parsing: Relationships between words
Phrase structure (the structure of a sentence)

#### Semantic Parsing

Turning natural language into executable code

### Parsing

Predicting linguistic structure from input sentence

#### Transition Models

Step through one word at a time.

#### Shift Reduce Parsing

Process words one by one
Queue of unprocessed words (or buffer)
Stack of partially processed words

at each point
shift -> queue to stack
reduce left -> top word on stack is head of second word (makes the two into an atom which is a target for other actions)
reduce right -> second word on stack is head of top word

Need to learn how to choose the right action with a classifier

##### What Features to Use?

The manual way was:

N words from the stack (the top words if there are joins)
N words from the buffer

Then the M left words of the top two words on stack
And the M right words of the top two words on stack

POS tags for all that.
The arc labels of all children or grandchildren

... I'm not sure how this is rendered in a nice way for the net

##### Tree Structure in NN

Syntactic Composition

###### Recursive Neural Network

The network is defined over trees instead of sequences.
So each node can take input from two things (either words or previous outputs).
Can also alter the weights used depending on the type of word.
Can use more complex layers (e.g. lstm or attention)

##### LSTM Approach

Can have 3 LSTMs operating over it.
One operates over the stack <- this is a tree though, see above
One operates over the buffer
One operates over the previous actions
They are then combined

#### Graph Based

Calculate probability of each branch

### Dependency Parsing

Can be useful for starting to understand relationships.
Related words are close in the tree (may be distant in the sentence).


## Sketch Based generation!

Coarse to Fine generation
First generate a "sketch" of the desired output
Then refine that sketch into the final form

I've frequently thought about this approach
