#### Object Detection

##### Pretraining with Weak Labels

Getting enough labelled images is hard.
Pretraining with weak labels (crowdsourced, unvalidated, weakly connected to objects within the scene) can improve the performance of object detection systems.
This [paper](https://arxiv.org/pdf/1805.00932.pdf) goes over using instagram hashtags to pretrain resnet architectures.

Unfortunately the data set is not publicly available.

##### Object Detection by inferring 3d shape

The creator of backpropogation has suggested that object detection systems would be more accurate if they started by trying to infer the 3d shape of an object from the 2d image.
This is closer to what we do as humans than the approach of encoding all different projections of an object.
This [series of blog posts](https://pechyonkin.me/capsules-1/) covers this approach.
At the moment this seems to use too much compute to be competative.

