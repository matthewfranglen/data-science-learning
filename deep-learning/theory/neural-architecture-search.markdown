#### Neural Architecture Search

The ideal architecture of a given network is an unsolved problem.
It is possible to measure the performance of a given network.
It is possible to generate a network layout via a neural network.
This means you can search for high performance networks using neural networks.

This [blog post](https://towardsdatascience.com/everything-you-need-to-know-about-automl-and-neural-architecture-search-8db1863682bf) introduces the subject.

