## NLP

There is a [fastai course on nlp](https://www.fast.ai/2019/07/08/fastai-nlp/) that would be worth doing.

### Language Models

Language Models are basically generic multitask problem solvers as they can be turned into many different tools via transfer learning.
This [paper](https://d4mucfpksywv.cloudfront.net/better-language-models/language-models.pdf) goes over that.

### Q&A Systems

#### GAN for Text (Q&A)

It's possible to generate text to train a Q&A system.
These facebook repos help [Unsupervised QA](https://github.com/facebookresearch/UnsupervisedQA) and [Unsupervised Machine Translation](https://github.com/facebookresearch/UnsupervisedMT).

### SQuAD

This [paper](https://arxiv.org/pdf/1606.05250.pdf) outlines squad.

### Summation

This [paper](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1174/reports/2762104.pdf) goes into different approaches for summarization.
There is a problem with getting good datasets of summaries with long form articles.

### Reference: Stronger Baselines for Neural MT. (Denkowski   Neubig 2017) https://www.aclweb.org/anthology/W17-3203.pdf
This covers advances in NLP and how the experimental systems designed by researchers may be difficult to compare to production systems.
The abstract talks about three methods which produce "much stronger experimental systems" - presumably this means closer to production or more comparable.
The core problem is evaluating techniques on very simple toy models may not accurately reflect the same changes in more complex production systems.
After all the problems identified in simple systems may've been solved in a different way to make the production system.

The three methods of improvement are:
 * Using Adam as the optimizer
 * Sub word translation via byte pair encoding (?)
 * Decoding with ensembles of independently trained models (?)

In the background it talks about how adam produces faster convergence however the models slightly under perform compared to annealing SGD (annealing being the reduction in learning rate, not the addition of momentum).
Adam is described as being "good enough".
All this makes me wonder if using some form of SGD for fine tuning would be appropriate?
The discussion of the difficulty that SGD has breaking symmetry makes me think that adding a small random component to the gradient modifications could help too?

I should note that the SGD starts with a learning rate of 0.5, which is far higher than the fastai default of 0.03.
They use an interesting approach for SGD - train with a learning rate, record the performance, roll back to the best point and start again halving the learning rate.
That seems to be a more thorough version of the auto decrease scheduler.

They point out that restarting adam clears the parameter rates, causing it to optimise differently.
There is no associated cost with restarting SGD.

One interesting result:
> Adam with just 2 restarts and SGD-style rate annealing is actually both
> faster than the fully annealed SGD and obtains significantly better results
> in both perplexity and BLEU.
(the restarts are the points at which it resets the parameter weights)

The discussion of sub word translation basically states that splitting words into smaller tokens allows for a greater vocabulary as rare words are often decomposed (at least partially).
This then means that there is more information for the system to work with, leading to better scores across the board.

The ensembling approach they talk about is to train multiple models and average the predictions at the word level.
Apparently ensembling by averaging is quite well known.
This feels a bit like the random forest approach.

They also talk about using distillation (whih I believe is to train a small model on the output of a larger model, or an ensemble of models).
Using distillation can produce a model which is easier to manage in production.
