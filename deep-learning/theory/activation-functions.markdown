#### Adaptive Softmax

Softmax is a non linearity which has the following qualities:

 * The sum of all outputs is 1
 * All outputs are positive (and so must be between 0 and 1 inclusive)

This involves every value in an activation layer so is it efficient to calcuate on a GPU?
The fact that [this implementation of adaptive softmax](https://github.com/facebookresearch/adaptive-softmax) exists suggests not.
It would be good to understand why this is better on a GPU and what drawbacks it has.
