#### One Shot Learning

Learning to recognize something from a single impression is hard.
It's described as one shot learning and [this blog post](https://sorenbouma.github.io/blog/oneshot/) goes over using it for recognizing letters.
