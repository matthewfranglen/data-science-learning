Using the Output Embedding. (Press and Wolf 2016) https://arxiv.org/pdf/1608.05859.pdf

This is a paper that discusses how the topmost matrix of a neural network language model constitutes a valid embedding.
This is a particularly salient topic for me, having used BERT to generate vectors for contexts (see https://gitlab.com/matthewfranglen/bert-question-answering - which is private).
The implementation of the BERT work was based on this stack question: https://stackoverflow.com/questions/55619176/how-to-cluster-similar-sentences-using-bert
This paper discusses how you can tie the input embedding to the output embedding, and how you can create smaller models based on these embeddings without losing accuracy.

It starts by describing how the input to a network is converted into an embedding by the first layer of the network.
It is then this embedding which the network operates over (by iteratively computing new matrices), producing probabilities in the final layer.
Apparently using LSTM (long short term memory) results in the output matrix being the same shape as the first embedding input.
The paper points out how similarities in the behaviour of these embeddings is desirable - e.g. synonyms in the input should be treated similarly, and synonyms in the output should have similar scores.

They then describe the results of tying these embeddings together, and how the results of doing this differ for different kinds of network (apparently it is actively harmful for word2vec).
It touches on how the gradient learning approach will naturally only update the input embedding for the current word being considered, which leads to rare words receiving few updates to their embeddings.
In contrast the output embeddings are updated for every word on every iteration.
(I should point out that the use of the term "word" is referring to a token which could be a subword part).
When the matrix is tied the updates to the matrix are similar to that for the output matrix, except for the one word which is the input.
(when discussing the performance of word2vec it was mentioned that the output matrix was lower value than the input matrix, which may be why the performance of it is harmed by tying).

For translation it appears there is an intermediate form which represents the "decoded" meaning.
This means that you have Language A -> decoded -> Language B, which means that the first arrow is the decoder and the second arrow is the encoder.
They propose three way weight tying between the input to the encoder, the output of the encoder and the input of the decoder (but not the output).

In the results it's rather interesting.
If my reading of the tables is correct, the test time perplexity scores are worse (higher) while the validation and test scores are better than the untied model.
It also shows that the regularization they proposed (which I have not discussed) leads to worse scores when used without the weight tying, and better when used with the weight tying.
I guess it is an obvious insight; some forms of regularization can only be used when coupled with some specific techniques in the network.

They claim that the worse scores at test time indicate less overfitting of the network because the total number of parameters is lower.
The translation scores are more about demonstrating that a significantly smaller (28% or 52% smaller) network can produce results that are very similar to the larger network.
It occurs to me that while weight tying may reduce the size of the network parameters the number of FLOPS required to compute the answer is not altered.
