Dropconnect. (Wan et al. 2013) (link is access forbidden, found http://yann.lecun.com/exdb/publis/pdf/wan-icml-13.pdf instead)
This paper introduces dropconnect which is a generalization of dropout.
This is intended for regularization of large fully connected layers.
The difference is that instead of zeroing the activation, dropconnect zeros the _weights_.

This starts by introducing the case for regularization, large networks can overfit even large datasets.
The number of parameters in a network will vastly outnumber all but the largest datasets.
For example, I consider efficientnet b0 to be small and that has 5 million parameters.
The open images dataset is very large and that has 9 million images.
Using regularization means that larger regularized networks outperform smaller non regularized networks.

It then introduces dropout and mentions that the prevailing theory for why it works is that it prevents collusion between network weights to memorize training examples.
What is interesting is that this paper claims that dropout is only suitable for fully connected layers - in the dropout paper it said that convolutions could use dropout.
This does seem natural, after all convolutions are much smaller and collusion between the weights of them is part of the point (they detect relationships after all).
It's interesting to see though, after all there arn't that many fully connected layers in a convolutional network.

It has an interesting visualization that shows that the effects of dropout can result in a strong pattern due to matrix multiplication.
This is because the masks are applied by dimension (is that correct?) so that the dropout applies to the previous layer and the current layer and forms a checkerboard effect.
By doing dropout over the weights (and biases) it breaks this correlation.
In this way drop connect becomes a generalization to the full connection structure.

One implementation detail worthy of note is that the mask applied to the weights must change for each entry in the mini-batch, otherwise drop connect does not apply enough regularization.
In practice this means that the mask will have an additional dimension for the batch entries, making it larger and increasing the memory requirements.

The means by which the network is compensated for dropconnect during training is not at all clear to me.
It points out that the mathematical assumption of dropout that allows an easy training -> inference alteration is broken for some things.
It uses a gaussian distribution to handle this for some reason.
I guess I need more math.
There are performance stats that show that the CUDA implementation of this approach is approximately 10x slower than BLAS implemented matrix multiplication.
Performance wise the drop connect mask can become extremely large - a 4096 x 4096 layer with 128 items in the batch is 2,147,483,648 mask values.
If they were stored as doubles (4 bytes) then that would be 8GB.
Fundamentally the mask is a bunch of 0 and 1 so it can be stored as a bitmask, reducing the memory requirements by 32 times (8G -> 256M).

The experiment results mention that dropconnect converges slower than dropout.
Broadly the experimental results show that dropconnect and dropout are competative and sometimes dropconnect is better, other times dropout is.
The experiments sometimes beat the SOTA without using either as well, so the augmentation and network architecture are significant factors in the performance.

