The fastai course covers the importance of initialization in lesson 9.
Basically a poorly initialized network will cause activations to either spiral to infinity (nan really) or to zero.
This makes it impossible to train the network.

A well initialized network should preserve the average amplitude of the activation that passes through it, allowing each layer to train appropriately.

https://course.fast.ai/videos/?lesson=9

In the lesson six different ways of initializing a network are covered.
Here are three:

[Self-Normalizing Neural Networks](https://arxiv.org/abs/1706.02515)
This has some extremely complex math involved which makes it impractical.

[All you need is a good init](https://arxiv.org/abs/1511.06422)
This takes an empirical approach to determining the weights.

[Fixup Initialization](https://arxiv.org/abs/1901.09321)
Not sure about the specifics on this one.
