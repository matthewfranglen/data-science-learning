https://ruder.io/optimizing-gradient-descent/index.html#whichoptimizertochoose

This is a discussion of gradient descent algorithms.

It starts by discussing the general approach to calculating gradients and applying them:

 * Batch gradient descent - apply updates once per epoch, accumulate gradients for all items in epoch
     Converges on local minima

 * Stochastic gradient descent (SGD) - apply updates once per batch
     This can cause the loss to get worse
     Can cause it to "jump" to new areas that may have a better local minima
     Complicates convergence
     - decrease learning rate over time to improve convergence

 * Mini batch gradient descent - apply updates every N batches
     "takes best of both worlds"
     reduces variance of the batches, leading to better convergence
     Can be referred to as SGD

Learning rate relates to the approach as well, as does the learning rate schedule (how you change the rate as you progress - either through batches or thresholds).
The same learning rate applies to all parameters (not true - see fast ai using a discriminative learning rate - maybe this would be a good area of investigation).
Could updating rarely occurring parameters more heavily be beneficial?

"Saddle points" cause problems with gradient descent.
These are points where the loss in one dimension slopes up, while another dimension slopes down.
This can lead to a nearly flat loss landscape in all directions.

Now it moves onto the different algorithms: https://ruder.io/optimizing-gradient-descent/index.html#gradientdescentoptimizationalgorithms

 * Momentum - partially apply the last update to the next update

 * Nesterov accelerated gradient - smarter momentum, you calculate the update based on where you _will be afterwards_ and then apply momentum based on that.
     This seems like a really neat idea - measuring the slope based on how you are adjusting the parameters.

 * Adagrad - vary learning rate by updating parameters for frequently occurring features less, and rarely occurring features more
     This seems a bit like TF/IDF for features.
     Presumably features can also be the inner life of the network - how to track how often a channel is activated deep in the network?

     This works by holding a specific learning rate per parameter which is updated each time that parameter is updated.
     The downside of this is the value held per parameter is similar to the total number of times it has been seen, which grows without bound.
     This causes the learning rate to shrink without bound.

 * Adadelta - attempt to fix the decreasing learning rate of adagrad by limiting the parameter learning rate to a window of past batches
     (actual window not used, it decays the previous value, a bit like momentum)
     We have come full circle now.
     Will there be a nesterov adadelta?

 * RMSprop - another approach to fixing adagrad
     This also divides the learning rate by "an exponentially decaying average of squared gradients"
     I need to think about this a bit to understand that.
     I guess that means that when the surface is flat the learning rate increases?
     May be a way to avoid jumping too far when the surface is irregular and slopes are steep.

 * Adaptive Moment Estimation (Adam) - also does adaptive rates per parameter
     This has two exponentially decaying averages - the squared gradients and the gradients
     This is like "a heavy ball with friction"

 * AdaMax - adaptation to adam
     I don't really understand it

 * Nadam - adam is RMSprop and momentum
     Nadam is Nesterov accelerated gradient applied to adam
     P.S. called it

 * AMSGrad - apparently all the insanity above can be worse than SGD with momentum for convergence on things like object recognition
     Apparently the exponential moving average of past squared gradients is the problem
     The short term memory becomes a problem (it solved a different problem though - one of becoming unable to learn at all)
     The batches themselves vary in the gradients they produce, batches with bigger gradients can be rare so they get forgotten
     AMSGrad uses the maximum of past squared gradients rather than the exponential average
     This means the step size does not increase
     The improvement over adam is NOT CLEAR CUT - it can be worse

https://ruder.io/optimizing-gradient-descent/index.html#visualizationofalgorithms
Visualization

Adam is the best overall choice.

Can add gradient noise (annealed) to help escape local minima

[x] Reference: The Marginal Value of Adaptive Gradient Methods. (Wilson et al. 2017) https://arxiv.org/pdf/1705.08292.pdf
This is a review of the gradient descent algorithms.
It compares the adaptive approaches which were described in the Overview (above) and compares them to plain gradient descent or stochastic gradient descent.
The abstract talks of a constructed data set which is linearly seperable for which GD / SGD converge to zero error while Adam et al get test errors arbitrarily close to 50%.
It also mentions how the adaptive optimizers produce results which generalize worse.

SO this seems pretty crushing.
Why not use SGD for everything?
Because it's slow?
Could SGD be used for convergence after initially moving through the space with Adam et al?

The paper does mention that it will introduce a scheme for tuning learning rates and decays for SGD that perform well.

To my mind this paper is a big deal and it came out 2 years ago.
If the results were reproducible and held up then they would be incorporated into things like fastai.

They are using relatively simple models some of which achieve SOTA results.
For example they have an RNN.
Presumably this paper predates BERT / ELMO?
The models do not seem to be large (hence my comment about relatively simple - 2 layer / 3 layer)

The learning rate schedule they use seems interesting to me.
They assess the accuracy of the validation and if the next epoch does not improve on that then the learning rate is decayed by a constant factor.
They train for 250 epochs!

It does feel like this could be achieved in fastai with a callback (to handle the decay of learning rate).
There is probably also a SGD optimizer.

There is a definition of perplexity (this term turned up in the first paper I reviewed, and I did not understand the definition):
    perplexity (the exponential of the average loss)

Turns out that SGD / momentum based approaches will find smoother areas than Adam et al which can find quite spiky areas.
The spiky areas generalize poorly.

I could consider using SGD / nesterov etc for fine tuning in future?

https://medium.com/@lessw/new-state-of-the-art-ai-optimizer-rectified-adam-radam-5d854730807b

This covers the Rectified Adam optimizer which is the recommended one to use with flat cosine annealing.
The funamental problem with optmizers like Adam is that they make poor decisions at the start of training when they lack data on the parameter movements.
This causes them to jump around, landing in sub optimal places by the time they learn enough to learn.

The authors of rectified adam (radam) tested this by just turning off the contributions from adaptive weights for the first 2000 iterations, creating adam-2k.
This performed better than regular adam when used with a high initial learning rate.
They took this as confirmation that the knowledge of the weight movements was the problem.

They then wanted to create an algorithm that would determine if enough was known about the weights to use them in calculating the update.
This is the rectification term that gradually increases the contribution that the weight knowledge makes to parameter updates.

I have been testing this against plain Adam.
I was running some very short tests which did not show much variance.
One thing that was noticeable was that Rectified Adam did better with a higher learning rate than Adam (3e-2 for radam compared to 3e-3 for adamw).
I can update this when I have some more results (I really should've run the evaluation on a GPU...)
