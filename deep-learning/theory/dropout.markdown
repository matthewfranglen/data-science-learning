Dropout. (Srivastava et al. 2014) http://jmlr.org/papers/volume15/srivastava14a.old/srivastava14a.pdf (30 pages!)

This is the paper that introduced dropout.
Dropout is the process of randomly excluding parameters in a network during training to prevent over specialization.
The parameters are chosen randomly from each layer for each batch, and a parameter controls the chance of a node being selected.
Because some percentage of weights is excluded from contributing during training, the network must change between test and evaluation to counteract the effect on the outputs.
If 50% of the nodes are zeroed out then the average output weight of a node is 2x what it would be without dropout.
If dropout was disabled without addressing this then the network would be broken as the activations would be overly influenced by the dropout trained parameters.

In this paper there is the suggestion of applying dropout to the input activations, however I believe that has fallen out of favour.

The author describes the process of dropout as creating a large number of thinned networks.
This is equated to training multiple different models and then ensembling the results of them.
I see this as being similar to data augmentation where input data is permuted to become one of many possible input values.

An equivalence between idea of dropout and sexual reproduction is suggested.
Sexual reproduction involves the commingling of two separate sets of genes.
This means that any large genes are likely broken by having half of the genes replaced.
So sexual reproduction may be selecting for genes that can function well alone or only depend on a small number of other genes.
Dropout is like this because it prevents large parameter groups from forming as any part of that group could be dropped out.
This makes the network more robust as it is made up of smaller more independent parts.

The paper is quite mixed, it introduces several different techniques which are then combined with dropout.
One interesting one is max-norm regularization which projects the parameters as a vector and imagines a sphere with a radius R.
If the vector exceeds the sphere then it is scaled back.
This maintains the ratios between the parameters while preventing them from becoming very big.
The paper states that this is useful because it permits far greater learning rates without weights exploding.

It also talks about maxout as an alternative activation function compared to relu, however it does not give details.

It's really difficult to see why dropout was missed.
This paper talks about significant improvements across many different domains (image, text, sound, genetics) and the graphs show something like a halving of error (for MNIST).
It got SOTA for nearly every task it was applied to.

It mentions baesian neural networks which apparently were the previously accepted way to average over the space of many neural network architectures.
The baesian approach is not restricted to treating all possible architectures equally, and instead can weight the architectures based on the prior and how well the model fits the data.
Apparently they are slow to train (and evaluate) and difficult to scale.
Baesian networks were used for the genetic task and the difference in size is remarkable - the baesian network is 10s of parameters per layer (compared to 1000s in the dropout network, which is low now)!

They demonstrate the way that dropout breaks up co-adaptation between features by visualizing the features of a network with and without dropout.
The difference is quite striking - the network that lacks dropout is quite noisy across all features and they must work by combining together.
The network with dropout has features that are actually detecting clearly different things.
This also leads to less activation in the features.
The author suggests this is why the generalization performance of the dropout network is better.

It's interesting that the Restricted Boltzmann Machine (RBM) also produces features which are well defined, like the ones achieved from dropout.
I don't understand the description of that kind of model though.
Might be worth looking into more.

The paper talks about deterministic learning too.
Apparently there is a paper on it: A. Globerson and S. Roweis. Nightmare at test time: robust learning by feature deletion. In Proceedings of the 23rd International Conference on Machine Learning, pages 353–360.ACM, 2006.

The approach of amplifying the weights during training instead of reducing them during evaluation is mentioned.

During the conclusion it talks about a downside of dropout - that it makes training take much longer.
This is because you are really training a subset of the model during each pass.
I wonder if this has been addressed, as it was the "future work" thing at the end of the conclusion.

Overall I thought this was very thorough if a little long.
I did not read the appendix.
