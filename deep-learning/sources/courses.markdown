After speaking with Colin the following courses are of interest:

Standford CS230 and CS224n
The videos are on youtube.

[Carnegie Mellon NLP](https://www.youtube.com/watch?v=pmcXgNTuHnk&list=PL8PYTP1V4I8Ajj7sY6sdtmjgkt7eo2VMs)
23 videos on NLP about an hour each.

[Carnegie Mellon DL Channel](https://www.youtube.com/channel/UC8hYZGEkI2dDO8scT8C5UQA)
It looks like there are a lot of videos and they are up to date - at time of viewing there were videos that are a day old.
