Oskar linked me this paper: https://arxiv.org/pdf/1404.1100.pdf
which is a tutorial on principle component analysis.

The main aim of the paper is to show how pca works and thus dispel the magical thinking around it.

It starts by describing an ideal spring oscillating, and asks how the appropriate measurement dimensions could be deduced from N cameras that record the motion of a ball at the end.
Each of these cameras records a different path of the ball, in reality the ball is moving along a single dimension.
While this is a very simple example it is meant to illustrate that the measurements we take can represent a simpler pattern in fewer dimensions.

It then goes on to describe an orthonormal basis, which is a set of dimensions that can be expressed as vectors in N dimensional space where every vector is orthogonal to every other vector (north-south being orthogonal to east-west, extended to multiple dimensions). https://en.wikipedia.org/wiki/Orthonormality
It suggests that such a description of the measurements must exist.
The paper starts by saying that the naive basis would be to assume that each measurement is orthogonal to each other measurement - however is this really the case?
What orthonormal basis best describes the data?
What does best even mean?
How can we derive this orthonormal basis from our measurements?

PCA assumes that there is a linear relationship between the best orthonormal basis and the measurements that have been collected.
This means that the best orthonormal basis can be achieved with only linear combinations of the current measurements.
This is in contrast to things like using log or exponents of the measurements.

Making an assumption of linearity is not enough to select the best orthonormal basis.
We must have a desired outcome for the re-expression.
So the paper proposes the following outcomes:
 * Reducing noise (collapse potential dimensions that add little information)
     The signal to noise ratio should be good enough that measurements have more signal than noise!
     This leads to maximizing the variance in the data, as that is the direction with the strongest spread between values.
 * Reducing redundancy / dimension reduction
     Can you predict one measurement from another?
     Determine the covariance between dimensions to determine if you can predict one from the other - values near zero indicate a lack of correlation.
     Can measure the covariance of every dimension w.r.t. every other dimension using a matrix - the diagonal then represents the variance of a dimension (it's covariance compared to itself)
     A better matrix would have high values on the diagonal (= interesting dimensions) and low values elsewhere (= uncorrelated dimensions).

With these two facts, especially the matrix approach, we can determine the algorithm for PCA:

 * Determine the direction that has the greatest variance, and save that as the first dimension
 * Determine the next dimension that has the greatest variance AND is orthogonal to all established dimensions (at this point just the first one)
 * Repeat

This will produce the ordered list of dimensions where the earlier dimensions have greater variance (and are thus likely more interesting than) the other dimensions.
These are the principle components.

I need to understand eigenvectors.
Bleh.

Looks like singular value decomposition (SVD) is a more general solution to calculating PCA.
The two are so similar that the names are often used interchangeably.

It then talks about the limits of PCA - namely the dimensions must be linearly separable.
This is not the case with a cyclic measurement (they use the example of the motion of the cars on a ferris wheel).
The other restriction is the orthogonality of the dimensions - sometimes this is not as good either - if you want to track two separate contrails they do not have to be orthogonal.

We can describe how good the resulting set of dimensions is by using a loss function.
How well can you predict the original values from the reduced values?

I've stopped at the appendix.
