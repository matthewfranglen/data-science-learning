#### Clustering

The [Cluster Walktrap](https://www.rdocumentation.org/packages/igraph/versions/1.2.4.1/topics/cluster_walktrap) function is based on:
> This function tries to find densely connected subgraphs, also called communities in a graph via random walks. The idea is that short random walks tend to stay in the same community.
